package main

import (
	"bufio"
	"context"
	"docker.io/go-docker"
	"docker.io/go-docker/api/types"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/buddyspencer/chameleon"
	"gopkg.in/alecthomas/kingpin.v2"
	"gopkg.in/yaml.v2"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type Container struct {
	Name       string         `json:"name"`
	Cgroups    []int          `json:"cgroups"`
	Id         string         `json:"id"`
	Data       map[int][]Data `json:"data"`
	ReadBytes  int            `json:"readbytes"`
	WriteBytes int            `json:"writebytes"`
	Stack      string		  `json:"stack"`
	Instance   string		  `json:"instance"`
}

type Data struct {
	Executable			string				`json:"executable"`
	FullPath			string				`json:"fullpath"`
	Rchar				int					`json:"rchar"`
	Wchar				int					`json:"wchar"`
	Syscr				int					`json:"syscr"`
	Syscw				int					`json:"syscw"`
	ReadBytes			int					`json:"read_bytes"`
	WriteBytes			int					`json:"write_bytes"`
	CancelledWriteBytes	int					`json:"cancelled_write_bytes"`
}

type Prometheus_Data struct {
	WroteBytes			*prometheus.GaugeVec
	ReadBytes			*prometheus.GaugeVec
}

var (
	cli docker.APIClient
	err interface{}
	io = ""
	comm = ""
	exe = ""
	containers = []Container{}
	gauges = []Prometheus_Data{}
	proc = "/proc/"
	sysDocker = "/sys/fs/cgroup/blkio/docker/"
	nodeinfo = types.Info{}

	app = kingpin.New("dockproc", "Get the real IO from docker")
	hostsystem = app.Flag("hostsystem", "The place you mounted the hostsystem, f.e.: /host").String()
	show = app.Command("show", "Show the IO stats")
	tojson = show.Flag("json", "Print in json").Bool()
	toyaml = show.Flag("yaml", "Print in yaml").Bool()
	prom = app.Command("prometheus", "Run prometheus metrics")
	promport = prom.Flag("port", "Port for displaying metrics").Short('p').Int()
	prominterval = prom.Flag("interval", "Interval to reload the metrics").Short('i').Int()
)

func initPaths() {
	if *hostsystem != "" {
		if strings.HasSuffix(*hostsystem, "/") {
			*hostsystem = strings.TrimSuffix(*hostsystem, "/")
		}
		proc = *hostsystem + proc
		sysDocker = *hostsystem + sysDocker
	}

	io = proc + "%d/io"
	comm = proc + "%d/comm"
	exe = proc + "%d/exe"
}

func calcBytes(bytes int) string {
	fbytes := float64(bytes)
	kbytes := fbytes / 1024
	mbytes := kbytes / 1024
	gbytes := mbytes / 1024
	tbytes := gbytes / 1024

	if tbytes > 1 {
		return fmt.Sprintf("%fT", tbytes)
	} else if gbytes > 1 {
		return fmt.Sprintf("%fG", gbytes)
	} else if mbytes > 1 {
		return fmt.Sprintf("%fM", mbytes)
	} else if kbytes > 1 {
		return fmt.Sprintf("%fK", kbytes)
	}
	return fmt.Sprintf("%fB", fbytes)
}

func getData(pid int, container *Container) Data {
	d := Data{}
	file, err := os.Open(fmt.Sprintf(io, pid))
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan(){
		splitted := strings.Split(scanner.Text(), ": ")
		if strings.HasPrefix(splitted[0], "rchar") {
			d.Rchar, err = strconv.Atoi(splitted[1])
		}
		if strings.HasPrefix(splitted[0], "wchar") {
			d.Wchar, err = strconv.Atoi(splitted[1])
		}
		if strings.HasPrefix(splitted[0], "syscr") {
			d.Syscr, err = strconv.Atoi(splitted[1])
		}
		if strings.HasPrefix(splitted[0], "syscw") {
			d.Syscw, err = strconv.Atoi(splitted[1])
		}
		if strings.HasPrefix(splitted[0], "read_bytes") {
			d.ReadBytes, err = strconv.Atoi(splitted[1])
			container.ReadBytes += d.ReadBytes
		}
		if strings.HasPrefix(splitted[0], "write_bytes") {
			d.WriteBytes, err = strconv.Atoi(splitted[1])
			container.WriteBytes += d.WriteBytes
		}
		if strings.HasPrefix(splitted[0], "cancelled_write_bytes") {
			d.CancelledWriteBytes, err = strconv.Atoi(splitted[1])
		}
		if err != nil {
			log.Fatal(err)
		}
	}
	file.Close()

	file, err = os.Open(fmt.Sprintf(comm, pid))
	if err != nil {
		log.Fatal(err)
	}
	scanner = bufio.NewScanner(file)
	for scanner.Scan() {
		d.Executable = scanner.Text()
	}
	file.Close()

	p, _ := os.Readlink(fmt.Sprintf(exe, pid))
	d.FullPath = p

	return d
}

func getInformation() []Container {
	containers = []Container{}
	c, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	for _, container := range c {
		labels := container.Labels
		initC := Container{}
		initC.Instance = nodeinfo.Name
		initC.Name = container.Names[0]
		initC.Id = container.ID
		initC.Data = make(map[int][]Data)
		initC.Stack = labels["com.docker.stack.namespace"]

		file, err := os.Open(fmt.Sprintf("%s/%s/cgroup.procs", sysDocker, initC.Id))
		if err != nil {
			log.Fatal(err)
		}
		scanner := bufio.NewScanner(file)
		for scanner.Scan(){
			pid, err := strconv.Atoi(scanner.Text())
			if err != nil {
				log.Fatal(err)
			}
			initC.Cgroups = append(initC.Cgroups, pid)
		}
		file.Close()

		for _, pid := range initC.Cgroups {
			initC.Data[pid] = append(initC.Data[pid], getData(pid, &initC))
		}
		containers = append(containers, initC)
	}
	return containers
}

func clearProm() {
	for _, x := range gauges {
		prometheus.Unregister(x.WroteBytes)
		prometheus.Unregister(x.ReadBytes)
	}

	gauges = []Prometheus_Data{}
}

func preparePromData() {
	if len(gauges) != 0 {
		clearProm()
	}
	log.Info("Get current stats...")
	wroteBytes := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "wroteBytes",
		Help: "Bytes wrote to host",
	}, []string{"stack", "container", "executable", "executable_path", "pid", "instance"})
	readBytes := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "readBytes",
		Help: "Bytes read from host",
	}, []string{"stack", "container", "executable", "executable_path", "pid", "instance"})
	getInformation()

	for _, x := range containers {
		for _, pid := range x.Cgroups {
			for _, data := range x.Data[pid] {
				d := Prometheus_Data{}
				d.WroteBytes = wroteBytes
				d.ReadBytes = readBytes
				d.WroteBytes.WithLabelValues(x.Stack, x.Name, data.Executable, data.FullPath, fmt.Sprintf("%d", pid), x.Instance).Set(float64(data.WriteBytes))
				d.ReadBytes.WithLabelValues(x.Stack, x.Name, data.Executable, data.FullPath, fmt.Sprintf("%d", pid), x.Instance).Set(float64(data.ReadBytes))

				prometheus.Register(d.WroteBytes)
				prometheus.Register(d.ReadBytes)
				gauges = append(gauges, d)
			}
		}
	}
}

func runProm() {
	initPaths()
	preparePromData()
	if *prominterval == 0 {
		*prominterval = 1
	}

	gocron.Every(uint64(*prominterval)).Minute().Do(preparePromData)
	go gocron.Start()

	r := mux.NewRouter()
	r.Handle("/metrics", promhttp.Handler())
	log.Panic(http.ListenAndServe(fmt.Sprintf(":%d", *promport), r))
}

func showStuff() {
	initPaths()
	getInformation()
	if *tojson {
		d, err := json.Marshal(containers)
		if err != nil {
			log.Fatal(err)
		}
		jsonStr := string(d)
		fmt.Println(jsonStr)
	}
	if *toyaml {
		d, err := yaml.Marshal(containers)
		if err != nil {
			log.Fatal(err)
		}
		yamlStr := string(d)
		fmt.Println(yamlStr)
	}
	if !*toyaml && !*tojson {
		for _, container := range containers {
			for i := range container.Cgroups {
				for _, data := range container.Data[container.Cgroups[i]] {
					fmt.Printf("name: %s executable: %s id: %s pid: %s\n", chameleon.Lightcyan(container.Name), chameleon.Green(data.Executable), chameleon.Green(container.Id), chameleon.Lightgreen(container.Cgroups[i]))
					fmt.Printf("%s: %d\n", chameleon.Lightgreen("rchar"), data.Rchar)
					fmt.Printf("%s: %d\n", chameleon.Lightgreen("wchar"), data.Wchar)
					fmt.Printf("%s: %d\n", chameleon.Lightgreen("syscr"), data.Syscr)
					fmt.Printf("%s: %d\n", chameleon.Lightgreen("syscw"), data.Syscw)
					fmt.Printf("%s: %s\n", chameleon.Lightgreen("read_bytes"), calcBytes(data.ReadBytes))
					fmt.Printf("%s: %s\n", chameleon.Lightgreen("write_bytes"), calcBytes(data.WriteBytes))
					fmt.Printf("%s: %s\n", chameleon.Lightgreen("cancelled_write_bytes"), calcBytes(data.CancelledWriteBytes))
					fmt.Println(chameleon.Lightgray("---------------------------------"))
				}
			}
			fmt.Println("Overall data for ", chameleon.Lightcyan(container.Name))
			fmt.Printf("%s: %s\n", chameleon.Lightgreen("Write"), calcBytes(container.WriteBytes))
			fmt.Printf("%s: %s\n", chameleon.Lightgreen("Read"), calcBytes(container.ReadBytes))
			fmt.Println(chameleon.Lightgray("---------------------------------"))
		}
	}
}

func main() {
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	app.Version("0.3.0")
	cli, err = docker.NewEnvClient()
	nodeinfo, err = cli.Info(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case prom.FullCommand():
		runProm()
	case show.FullCommand():
		showStuff()
	}
}