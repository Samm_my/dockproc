# DockProc

## What's this about?
It looks up your _REAL_ written and read data. 
DockProc checks your `proc` for container pids and reads the wrote_bytes and read_bytes.
I created DockProc because `docker stats` doesn't show the correct block I/O if you write/read data from an nfs share.
It works on the process level, therefore it doesn't care about shares.

The available options are:
```
usage: dockproc [<flags>] <command> [<args> ...]

Get the real IO from docker

Flags:
  --help                   Show context-sensitive help (also try --help-long and --help-man).
  --hostsystem=HOSTSYSTEM  The place you mounted the hostsystem, f.e.: /host
  --version                Show application version.

Commands:
  help [<command>...]
    Show help.

  show [<flags>]
    Show the IO stats

  prometheus [<flags>]
    Run prometheus metrics
```

`show` prints the current stats of your containers. If you put `--json` or `--yaml` behind it, you get the data in the wanted format.

## Prometheus
It runs as a daemon with `prometheus`. You can now grab the data with prometheus and make some decent dashboards with grafana.
If you need a dashboard for grafana: [Dashboard](https://grafana.com/dashboards/9555 ) 


<img src="pictures/dockproc dashboard.png" alt="dashboard" width="30%" height="30%">

## Docker Container
DockProc is also available as container. You can run it with the command below.
```
docker run --pid="host" -d -v /:/host:ro -v /var/run/docker.sock:/var/run/docker.sock:ro -p 8080:8080 registry.gitlab.com/n0r1sk/dockproc:latest
```

## Suggestions?
If you have any suggestions for features or improvements, let [me](https://gitlab.com/buddyspencer) know.